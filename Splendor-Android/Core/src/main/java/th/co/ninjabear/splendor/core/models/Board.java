package th.co.ninjabear.splendor.core.models;

import java.lang.invoke.SwitchPoint;

/**
 * Created by Mekumi on 6/20/2017 AD.
 */

public class Board {

    private int blueGem;
    private int whiteGem;
    private int redGem;
    private int blackGem;
    private int greenGem;

    public Board(final int startingGemCount) {
        this.blueGem = startingGemCount;
        this.whiteGem = startingGemCount;
        this.redGem = startingGemCount;
        this.blackGem = startingGemCount;
        this.greenGem = startingGemCount;
    }

    public void takeGem(Gem gem) {

        switch (gem){
            case BLUE:
                blueGem--;
                break;
            case WHITE:
                whiteGem--;
                break;
            case RED:
                redGem--;
                break;
            case BLACK:
                blackGem--;
                break;
            case GREEN:
                greenGem--;
                break;
        }
    }


}
