package th.co.ninjabear.splendor.core.engine;

import th.co.ninjabear.splendor.core.models.Board;
import th.co.ninjabear.splendor.core.models.Gem;

public class Game {

    private Board board = new Board(7);

    public void take3(Gem gem1, Gem gem2, Gem gem3) {
        board.takeGem(gem1);
        board.takeGem(gem2);
        board.takeGem(gem3);
    }

}
