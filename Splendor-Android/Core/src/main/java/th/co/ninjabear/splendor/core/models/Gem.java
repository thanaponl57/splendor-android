package th.co.ninjabear.splendor.core.models;

public enum Gem {

    RED,
    GREEN,
    BLUE,
    WHITE,
    BLACK;

}
